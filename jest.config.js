/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	preset: "ts-jest",
	testEnvironment: "jsdom",
	testPathIgnorePatterns: ["/node_modules/", "/dist/"],
	collectCoverageFrom: ["**/*.{ts,tsx}"],
	coverageThreshold: {
		global: {
			branches: 64,
			functions: 39,
			lines: 39,
			statements: 39,
		},
	},
	moduleNameMapper: {
		"\\.scss$": "<rootDir>/node_modules/jest-css-modules",
		"^@components(.*)$": "<rootDir>/src/components$1",
		"^@stores(.*)$": "<rootDir>/src/stores$1",
		"^@ws(.*)$": "<rootDir>/src/ws$1",
		"^@base(.*)$": "<rootDir>/src$1",
	},
};

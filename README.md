# Crypto currency order book

![enter image description here](https://bitbucket.org/rinalds/crypto-orderbook/raw/ccec5f0d3f1d3b55507588c8c9c7210e54a88f43/.bitbucket/cover.png)

## Website

[Click here to check out the app online](https://thirsty-kepler-6e4c6f.netlify.app/)

## Test

1. yarn install

2. yarn serve to start dev environment

## Build

1. yarn install

2. yarn build

## For testing

-   Validate typescript - yarn validate-ts

-   Validate eslint - yarn validate-lint

-   Validate both - yarn validate

-   Run tests - yarn test

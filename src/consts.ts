export enum OrderValue {
	Price,
	Size,
	Total,
}

export enum SupportedProducts {
	PI_XBTUSD = "PI_XBTUSD",
	PI_ETHUSD = "PI_ETHUSD",
}

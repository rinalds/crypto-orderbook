import { OrderValue } from "./consts";
import { TOrders, TOrder, TOrdersExtended, TOrderExtended } from "./types";

export function convertOrdersToOrdersMap(orders: TOrders) {
	const ordersMap = new Map<number, TOrder>();

	orders.forEach((order) => {
		ordersMap.set(order[OrderValue.Price], order);
	});

	return ordersMap;
}

export function extendOrdersWithTotal(
	ordersMap: Map<number, TOrder>,
	ascending: boolean,
	numberOfRows: number,
): TOrderExtended[] {
	return Array.from(ordersMap.values())
		.sort((a, b) => {
			if (ascending) {
				return a[OrderValue.Price] - b[OrderValue.Price];
			}
			return b[OrderValue.Price] - a[OrderValue.Price];
		})
		.slice(0, numberOfRows)
		.map((ask, index, array) => {
			const total = array
				.slice(0, index + 1)
				.reduce((a, curr) => a + curr[OrderValue.Size], 0);
			return [...ask, total];
		});
}

export function updateOrderMap(
	orders: TOrders,
	ordersMap: Map<number, TOrder>,
): void {
	orders.forEach((order) => {
		if (
			ordersMap.has(order[OrderValue.Price]) &&
			order[OrderValue.Size] === 0
		) {
			ordersMap.delete(order[OrderValue.Price]);
		} else if (order[OrderValue.Size] !== 0) {
			ordersMap.set(order[OrderValue.Price], order);
		}
	});
}

export function calculateHighestTotal(orders: TOrdersExtended): number {
	return orders.reduce((highest, order) => {
		if (order[OrderValue.Total] > highest) {
			return order[OrderValue.Total];
		}
		return highest;
	}, 0);
}

import { SupportedProducts } from "@base/consts";
import { OrderBook } from "@components/OrderBook/OrderBook";
import { getStoreContext } from "@stores/StoreContext";
import { getEnhancedMessageSenders } from "@ws/enhancedWsMessageSenders";
import { reaction } from "mobx";
import { observer } from "mobx-react-lite";
import React, { useCallback, useContext, useEffect } from "react";

export const OrderBookPage = observer(() => {
	const store = useContext(getStoreContext());

	const { orderBook: orderBookTransport } = getEnhancedMessageSenders();

	useEffect(() => {
		const disposeConnectionReaction = reaction(
			() => store.app.wsStatus,
			(status) => {
				if (status === "connected") {
					orderBookTransport.subscribe(SupportedProducts.PI_XBTUSD);
					store.orderBook.start();
					disposeConnectionReaction();
				}
			},
		);

		// Note to self: maybe move this in different file or maybe store
		const disposeReaction = reaction(
			() => store.app.appInFocus,
			(appInFocus) => {
				if (appInFocus) {
					return;
				}
				orderBookTransport.disconnect();
				store.orderBook.stop();
				store.popupManager.openPopup({
					text: "You have been disconnected from live feed.",
					buttonTitle: "Click here to reconnect",
					onClose: () => {
						store.popupManager.closePopup();
						orderBookTransport.reconnect();
						store.orderBook.start();
					},
				});
			},
		);

		return () => {
			store.orderBook.stop();
			disposeReaction();
			disposeConnectionReaction();
		};
	}, []);

	const handleSwitchProduct = useCallback(() => {
		orderBookTransport.switchProduct(SupportedProducts.PI_ETHUSD);
	}, []);

	return (
		<OrderBook
			highestTotal={store.orderBook.highestTotal}
			formattedSpread={store.orderBook.formattedSpread}
			bids={store.orderBook.bids}
			asks={store.orderBook.asks}
			onSwitchProduct={handleSwitchProduct}
			onRowLengthChange={store.orderBook.setNumberOfRows}
			availableSpace={store.app.availableSpace}
		/>
	);
});

export interface IDisposable {
	dispose: () => void;
}

export type TOrder = [number, number];
export type TOrderExtended = [number, number, number];
export type TOrders = TOrder[];
export type TOrdersExtended = TOrderExtended[];

export type TFeed = {
	feed: string;
	product_id: string;
	bids: TOrder[];
	asks: TOrder[];
};

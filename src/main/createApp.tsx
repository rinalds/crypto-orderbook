import { observer } from "mobx-react-lite";
import React from "react";
import ReactDOM from "react-dom";

import { App } from "@base/main/App/App";
import { createOrderBookMessageHandler } from "@ws/message-handlers/handleOrderBook";
import {
	createStoreContext,
	getStore,
	getStoreContext,
} from "@stores/StoreContext";
import { createEnhancedMessageSenders } from "@ws/enhancedWsMessageSenders";

import { createWsConnection } from "../ws/createWsConnection";

export interface AppConfig {
	wsUrl: string;
}

export function createApp({ wsUrl }: AppConfig) {
	createStoreContext();
	const store = getStore();

	// -- start of WS setup
	const { sendMessage, dispose: disposeConnection } = createWsConnection(
		wsUrl,
		{
			onOpen: () => {
				store.app.setWsStatus("connected");
			},
			onClose: () => {
				store.app.setWsStatus("disconnected");
				store.popupManager.openPopup({
					text: "You have been disconnected",
					buttonTitle: "Click here to refresh page",
					onClose: window.location.reload,
				});
			},
		},
		[createOrderBookMessageHandler(store)],
	);

	createEnhancedMessageSenders(sendMessage);
	// -- end of WS setup

	// -- start of React setup
	const el = document.querySelector("#app");

	const StoreContext = getStoreContext();

	const ConnectedApp = observer(App);

	ReactDOM.render(
		<StoreContext.Provider value={store}>
			<ConnectedApp />
		</StoreContext.Provider>,
		el,
	);
	// -- end of React setup

	const dispose = () => {
		disposeConnection();
		store.dispose();
	};

	return {
		dispose,
	};
}

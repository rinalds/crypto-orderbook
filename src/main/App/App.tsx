import React, { useContext } from "react";
import cn from "classnames";

import { Popup } from "@components/Popup/Popup";
import { OrderBookPage } from "@base/pages/OrderBookPage";
import { getStoreContext } from "@stores/StoreContext";

import style from "./App.module.scss";

export const App = () => {
	const store = useContext(getStoreContext());

	return (
		<>
			<div
				className={cn([
					style.app,
					{
						[style.blur]: store.popupManager.openedPopup,
					},
				])}
			>
				<OrderBookPage />
			</div>
			{store.popupManager.openedPopup && (
				<Popup
					text={store.popupManager.openedPopup.text}
					buttonTitle={store.popupManager.openedPopup.buttonTitle}
					onClose={store.popupManager.openedPopup.onClose}
				/>
			)}
		</>
	);
};

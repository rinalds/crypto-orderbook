import {
	calculateHighestTotal,
	convertOrdersToOrdersMap,
	extendOrdersWithTotal,
	updateOrderMap,
} from "@base/orderUtils";
import { checkIfMobile } from "@base/utils";
import { action, computed, makeObservable, observable } from "mobx";
import { IDisposable, TOrder, TOrderExtended, TOrders } from "../types";

export class OrderBookStore implements IDisposable {
	public bidsMap: Map<number, TOrder> = new Map();

	public asksMap: Map<number, TOrder> = new Map();

	public throttledBidsMap: Map<number, TOrder> = new Map();

	public throttledAsksMap: Map<number, TOrder> = new Map();

	protected numberOfRows = 20;

	protected updateThrottle = checkIfMobile() ? 3000 : 1000;

	protected intervalId: number | undefined = undefined;

	public constructor() {
		makeObservable(this, {
			bidsMap: observable,
			asksMap: observable,

			bids: computed,
			asks: computed,
			highestTotal: computed,
			formattedSpread: computed,

			setBidsAndAsks: action,
			updateBidsAndAsks: action,
		});
	}

	public get bids(): TOrderExtended[] {
		return extendOrdersWithTotal(this.bidsMap, false, this.numberOfRows);
	}

	public get asks(): TOrderExtended[] {
		return extendOrdersWithTotal(this.asksMap, true, this.numberOfRows);
	}

	public get formattedSpread() {
		if (this.bids.length === 0 || this.asks.length === 0) {
			return "";
		}
		const spread = Math.abs(this.bids[0][0] - this.asks[0][0]);
		const spreadPercentage = (spread / this.bids[0][0]) * 100;

		return `${spread.toFixed(2)} (${spreadPercentage.toFixed(2)}%)`;
	}

	public get highestTotal(): number {
		const highestBidsTotal = calculateHighestTotal(this.bids);
		const highestAsksTotal = calculateHighestTotal(this.asks);

		return highestBidsTotal > highestAsksTotal
			? highestBidsTotal
			: highestAsksTotal;
	}

	public start() {
		window.clearInterval(this.intervalId);
		this.intervalId = window.setInterval(
			action(() => {
				this.bidsMap = new Map(this.throttledBidsMap);
				this.asksMap = new Map(this.throttledAsksMap);
			}),
			this.updateThrottle,
		);
	}

	public stop() {
		window.clearInterval(this.intervalId);
		this.bidsMap = new Map(this.throttledBidsMap);
		this.asksMap = new Map(this.throttledAsksMap);
	}

	public setBidsAndAsks(bids: TOrders, asks: TOrders) {
		this.bidsMap = convertOrdersToOrdersMap(bids);
		this.asksMap = convertOrdersToOrdersMap(asks);

		this.throttledBidsMap = new Map(this.bidsMap);
		this.throttledAsksMap = new Map(this.asksMap);
	}

	public updateBidsAndAsks(bids: TOrders, asks: TOrders) {
		updateOrderMap(bids, this.throttledBidsMap);
		updateOrderMap(asks, this.throttledAsksMap);
	}

	public setNumberOfRows = (numberOfRows: number) => {
		this.numberOfRows = numberOfRows;
	};

	public dispose() {
		this.stop();
	}
}

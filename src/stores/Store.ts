import { IDisposable } from "@base/types";
import { AppStore } from "./AppStore";
import { OrderBookStore } from "./OrderBookStore";
import { PopupManagerStore } from "./PopupManagerStore";

export class Store implements IDisposable {
	public orderBook = new OrderBookStore();

	public popupManager = new PopupManagerStore();

	public app = new AppStore();

	public dispose() {
		this.orderBook.dispose();
		this.app.dispose();
	}
}

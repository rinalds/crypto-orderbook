import React from "react";

import { Store } from "./Store";

let StoreContext: React.Context<Store>;
let store: Store;

export function createStoreContext(): void {
	store = new Store();
	StoreContext = React.createContext(store);
}

export function getStoreContext(): React.Context<Store> {
	if (!StoreContext) {
		throw new Error("Using StoreContext before initialized");
	}
	return StoreContext;
}

export function getStore(): Store {
	if (!store) {
		throw new Error("Using store before initialized");
	}
	return store;
}

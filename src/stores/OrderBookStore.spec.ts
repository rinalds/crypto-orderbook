import { convertOrdersToOrdersMap } from "@base/orderUtils";
import { TOrder, TOrders } from "../types";
import { OrderBookStore } from "./OrderBookStore";

const bids: TOrders = [
	[30000, 1100],
	[35000, 1200],
	[40000, 1300],
	[45000, 1400],
	[50000, 1500],
	[55000, 1600],
	[60000, 1700],
	[65000, 1800],
	[70000, 1900],
	[75000, 2000],
];

const asks: TOrders = [
	[30000, 1100],
	[35000, 1200],
	[40000, 1300],
	[45000, 1400],
	[50000, 1500],
	[55000, 6000],
	[60000, 1700],
	[65000, 1800],
	[70000, 1900],
	[75000, 2000],
];

const newBids: TOrders = [
	[30000, 0],
	[35000, 4000],
];

const newAsks: TOrders = [
	[30000, 0],
	[35000, 4000],
];

const updatedBids: TOrders = [[35000, 4000], ...bids.slice(2)];
const updatedAsks: TOrders = [[35000, 4000], ...asks.slice(2)];

const convertArrayToEntries = (m: TOrders) =>
	Array.from(convertOrdersToOrdersMap(m).entries());

const convertMapToEntries = (m: Map<number, TOrder>) => Array.from(m.entries());

describe("OrderBookStore", () => {
	it("should throttle order updating", () => {
		jest.useFakeTimers();

		const orderBook = new OrderBookStore();

		orderBook.setBidsAndAsks(bids, asks);

		orderBook.start();

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(bids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(asks),
		);

		orderBook.updateBidsAndAsks(newBids, newAsks);

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(bids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(asks),
		);

		jest.advanceTimersByTime(3000);

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(updatedBids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(updatedAsks),
		);

		orderBook.stop();

		jest.useRealTimers();
	});

	it("should stop throttling if stop()", () => {
		jest.useFakeTimers();

		const orderBook = new OrderBookStore();

		orderBook.setBidsAndAsks(bids, asks);

		orderBook.start();

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(bids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(asks),
		);

		orderBook.updateBidsAndAsks(newBids, newAsks);

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(bids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(asks),
		);

		orderBook.stop();

		expect(convertMapToEntries(orderBook.bidsMap)).toEqual(
			convertArrayToEntries(updatedBids),
		);
		expect(convertMapToEntries(orderBook.asksMap)).toEqual(
			convertArrayToEntries(updatedAsks),
		);

		jest.useRealTimers();
	});

	it("should return correct spread with formattedSpread", () => {
		const orderBook = new OrderBookStore();

		orderBook.setBidsAndAsks(bids, asks);

		expect(orderBook.formattedSpread).toBe("45000.00 (60.00%)");
	});

	it("should return highestTotal between bids and asks", () => {
		const orderBook = new OrderBookStore();

		orderBook.setBidsAndAsks(bids, asks);

		expect(orderBook.highestTotal).toBe(19900);
	});
});

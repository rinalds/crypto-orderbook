import { action, makeObservable, observable } from "mobx";

export interface TPopup {
	text: string;
	buttonTitle: string;
	onClose: () => void;
}

export class PopupManagerStore {
	public openedPopup: TPopup | undefined = undefined;

	public constructor() {
		makeObservable(this, {
			openedPopup: observable,
			openPopup: action,
			closePopup: action,
		});
	}

	public openPopup(popup: TPopup) {
		this.openedPopup = popup;
	}

	public closePopup() {
		this.openedPopup = undefined;
	}
}

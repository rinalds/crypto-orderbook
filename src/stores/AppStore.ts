import { IDisposable } from "@base/types";
import { debounce } from "debounce";
import { action, makeObservable, observable } from "mobx";

type WsStatus = "connected" | "connecting" | "disconnected";

export class AppStore implements IDisposable {
	public appInFocus = false;

	public wsStatus: WsStatus = "connecting";

	public availableSpace = {
		width: window.innerWidth,
		height: window.innerHeight,
	};

	public constructor() {
		makeObservable(this, {
			appInFocus: observable,
			wsStatus: observable,
			availableSpace: observable,
			setAppInFocus: action,
			setWsStatus: action,
			setAvailableSpace: action,
		});

		window.addEventListener("blur", this.onBlur);
		window.addEventListener("focus", this.onFocus);
		window.addEventListener("resize", this.onResize);
	}

	public setWsStatus(status: WsStatus) {
		this.wsStatus = status;
	}

	public setAvailableSpace(width: number, height: number) {
		this.availableSpace.width = width;
		this.availableSpace.height = height;
	}

	protected onBlur = () => {
		this.setAppInFocus(false);
	};

	protected onFocus = () => {
		this.setAppInFocus(true);
	};

	protected onResize = debounce(() => {
		this.setAvailableSpace(window.innerWidth, window.innerHeight);
	}, 500);

	public setAppInFocus(appInFocus: boolean) {
		this.appInFocus = appInFocus;
	}

	public dispose() {
		window.removeEventListener("blur", this.onBlur);
		window.removeEventListener("focus", this.onFocus);
		window.removeEventListener("resize", this.onResize);
	}
}

import { SupportedProducts } from "@base/consts";
import { TFeed } from "@base/types";
import { Store } from "@stores/Store";
import { TMessageHandler } from "@ws/createWsConnection";

export type TMessage = {
	event: "subscribe" | "unsubscribe";
	feed: string;
	product_ids: SupportedProducts[];
};

type CreateHandleOrderBook = (store: Store) => TMessageHandler;

function isFeed(data: unknown): data is TFeed {
	return (
		"feed" in (data as TFeed) &&
		"bids" in (data as TFeed) &&
		"asks" in (data as TFeed)
	);
}

export const createOrderBookMessageHandler: CreateHandleOrderBook =
	(store: Store) => (data) => {
		if (!isFeed(data)) {
			return;
		}

		if (data.feed.endsWith("_snapshot")) {
			store.orderBook.setBidsAndAsks(data.bids, data.asks);
		} else {
			store.orderBook.updateBidsAndAsks(data.bids, data.asks);
		}
	};

export function createOrderBookServerMessageSenders(
	sendMessage: (data: unknown) => void,
) {
	let currentProductId: SupportedProducts = SupportedProducts.PI_XBTUSD;

	const subscribe = (productId: SupportedProducts) =>
		sendMessage({
			event: "subscribe",
			feed: "book_ui_1",
			product_ids: [productId],
		});

	const unsubscribe = (productId: SupportedProducts) =>
		sendMessage({
			event: "unsubscribe",
			feed: "book_ui_1",
			product_ids: [productId],
		});

	const switchProduct = (productId: SupportedProducts) => {
		unsubscribe(currentProductId);
		subscribe(productId);
		currentProductId = productId;
	};

	const disconnect = () => unsubscribe(currentProductId);
	const reconnect = () => subscribe(currentProductId);

	return {
		subscribe,
		unsubscribe,
		switchProduct,
		disconnect,
		reconnect,
	};
}

export type TMessageHandler = (
	data: unknown,
	sendMessage: (data: unknown) => void,
) => void;

interface WsEventHandlers {
	onOpen: () => void;
	onClose: () => void;
}

export function createWsConnection(
	wsUrl: string,
	eventHandlers: WsEventHandlers,
	messageHandlers: TMessageHandler[],
) {
	const ws = new WebSocket(wsUrl);

	const sendMessage = (msg: unknown) => ws.send(JSON.stringify(msg));

	ws.addEventListener("open", eventHandlers.onOpen);

	ws.addEventListener("error", eventHandlers.onClose);
	ws.addEventListener("close", eventHandlers.onClose);

	ws.addEventListener("message", (event) => {
		const data: unknown = JSON.parse(event.data);

		messageHandlers.forEach((messageHandler) => {
			messageHandler(data, sendMessage);
		});
	});

	const dispose = () => {
		ws.close();
	};

	return {
		sendMessage,
		dispose,
	};
}

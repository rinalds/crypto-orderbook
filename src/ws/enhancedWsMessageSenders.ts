import { createOrderBookServerMessageSenders } from "./message-handlers/handleOrderBook";

export interface WsActions {
	orderBook: ReturnType<typeof createOrderBookServerMessageSenders>;
}

let wsActions: WsActions;

export function createEnhancedMessageSenders(
	sendMessage: (data: unknown) => void,
) {
	wsActions = {
		orderBook: createOrderBookServerMessageSenders(sendMessage),
	};
}

export function getEnhancedMessageSenders(): WsActions {
	if (!wsActions) {
		throw new Error("Using enhanced message senders before initialized");
	}
	return wsActions;
}

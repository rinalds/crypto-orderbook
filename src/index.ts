import { createApp } from "./main/createApp";

createApp({
	wsUrl: "wss://www.cryptofacilities.com/ws/v1",
});

import React, { useEffect, useRef } from "react";

import { OrderPage } from "@components/OrderPage/OrderPage";
import { OrderBookHeader } from "@components/OrderBookHeader/OrderBookHeader";
import { Button } from "@components/Button/Button";
import { round } from "@base/utils";
import { TOrdersExtended } from "@base/types";

import style from "./OrderBook.module.scss";

interface OrderBookProps {
	formattedSpread: string;
	highestTotal: number;
	bids: TOrdersExtended;
	asks: TOrdersExtended;
	availableSpace: {
		width: number;
		height: number;
	};
	onSwitchProduct: () => void;
	onRowLengthChange: (numberOfRows: number) => void;
}

const getStyleVariable = (varName: string, elStyle: CSSStyleDeclaration) =>
	parseInt(elStyle.getPropertyValue(varName), 10);

export const OrderBook = ({
	highestTotal,
	formattedSpread,
	bids,
	asks,
	availableSpace,
	onSwitchProduct,
	onRowLengthChange,
}: OrderBookProps) => {
	const el = useRef<HTMLDivElement>(null);

	const { width, height } = availableSpace;

	const isSmallScreen = width <= 600;

	useEffect(() => {
		const rootStyle = getComputedStyle(el.current!);
		const rowHeight = getStyleVariable(
			"--order-book-row-height",
			rootStyle,
		);
		const headerHeight = getStyleVariable(
			"--order-book-header-height",
			rootStyle,
		);
		const footerHeight = getStyleVariable(
			"--order-book-footer-height",
			rootStyle,
		);

		// table header is same height as rowHeight
		let availableHeight = height - rowHeight - headerHeight - footerHeight;

		if (isSmallScreen) {
			availableHeight -= headerHeight;
		}

		const numberOfRows = isSmallScreen
			? round(availableHeight / 2 / rowHeight)
			: round(availableHeight / rowHeight);

		onRowLengthChange(numberOfRows);
	}, [isSmallScreen, height]);

	return (
		<div className={style.orderBook}>
			<OrderBookHeader
				spread={formattedSpread}
				hideSpread={isSmallScreen}
			/>
			<div className={style.orderBookContent} ref={el}>
				<OrderPage
					orders={bids}
					highestTotal={highestTotal}
					color="green"
					reverseColumnOrder={!isSmallScreen}
					showHeader={!isSmallScreen}
				/>
				{isSmallScreen && (
					<OrderBookHeader spread={formattedSpread} onlySpread />
				)}
				<OrderPage
					orders={asks}
					highestTotal={highestTotal}
					color="red"
					reverseRowOrder={isSmallScreen}
				/>
			</div>
			<div className={style.footer}>
				<Button title="Toggle Feed" onClick={onSwitchProduct} />
			</div>
		</div>
	);
};

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import { TOrdersExtended } from "@base/types";

import { OrderBook } from "./OrderBook";

jest.mock("react", () => ({
	...jest.requireActual<typeof React>("react"),
	useRef: () => ({
		current: {
			getBoundingClientRect: () => ({
				width: 200,
				height: 200,
			}),
		},
	}),
}));

const bids: TOrdersExtended = [
	[30000, 1100, 1100],
	[35000, 1200, 1200],
	[40000, 1300, 1300],
	[45000, 1400, 1400],
	[50000, 1500, 1500],
	[55000, 1600, 1600],
	[60000, 1700, 1700],
	[65000, 1800, 1800],
	[70000, 1900, 1900],
	[75000, 2000, 2000],
];

const asks: TOrdersExtended = [
	[30000, 1100, 1100],
	[35000, 1200, 1200],
	[40000, 1300, 1300],
	[45000, 1400, 1400],
	[50000, 1500, 1500],
	[55000, 1600, 1600],
	[60000, 1700, 1700],
	[65000, 1800, 1800],
	[70000, 1900, 1900],
	[75000, 2000, 2000],
];

describe("OrderBook", () => {
	let globalGetComputedStyle: any;

	beforeAll(() => {
		globalGetComputedStyle = window.getComputedStyle;

		window.getComputedStyle = () =>
			({
				getPropertyValue: () => "30",
			} as any);
	});

	afterAll(() => {
		window.getComputedStyle = globalGetComputedStyle;
	});

	it("should test if OrderBook adapts to width and height changes and fire all onSwitchProduct onRowLengthChange", () => {
		const onSwitchProductMock = jest.fn();
		const onRowLengthChangeMock = jest.fn();

		const { container, rerender } = render(
			<OrderBook
				formattedSpread="123%"
				highestTotal={123}
				bids={bids}
				asks={asks}
				onSwitchProduct={onSwitchProductMock}
				availableSpace={{
					width: 1000,
					height: 1000,
				}}
				onRowLengthChange={onRowLengthChangeMock}
			/>,
		);

		expect(container.querySelectorAll(".orderPageRow").length).toBe(20);
		expect(
			container.querySelector(".orderBookHeader .spread")?.innerHTML,
		).toBe("Spread 123%");

		fireEvent.click(container.querySelector(".footer button")!);

		expect(onSwitchProductMock).toBeCalledTimes(1);
		expect(onRowLengthChangeMock).toHaveBeenLastCalledWith(30);

		rerender(
			<OrderBook
				formattedSpread="123%"
				highestTotal={123}
				bids={bids}
				asks={asks}
				onSwitchProduct={onSwitchProductMock}
				availableSpace={{
					width: 599,
					height: 800,
				}}
				onRowLengthChange={onRowLengthChangeMock}
			/>,
		);

		expect(onRowLengthChangeMock).toHaveBeenLastCalledWith(11);
	});
});

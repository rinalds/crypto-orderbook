import React from "react";
import classNames from "classnames";

import { OrderValue } from "@base/consts";
import { TOrdersExtended } from "@base/types";

import style from "./OrderPage.module.scss";
import { OrderPageHeader } from "./components/OrderPageHeader";
import { OrderPageRow } from "./components/OrderPageRow";

export interface OrderPageProps {
	orders: TOrdersExtended;
	highestTotal: number;
	color: "red" | "green";
	reverseColumnOrder?: boolean;
	showHeader?: boolean;
	reverseRowOrder?: boolean;
}

export function OrderPage({
	orders,
	highestTotal,
	color,
	reverseColumnOrder = false,
	showHeader = true,
	reverseRowOrder = false,
}: OrderPageProps) {
	return (
		<div
			className={classNames(style.orderPage, style[color], {
				[style.reversedColumnOrder]: reverseColumnOrder,
				[style.reversedRowOrder]: reverseRowOrder,
			})}
		>
			{showHeader && <OrderPageHeader />}
			<div className={style.orderPageRows}>
				{orders.map((bid) => (
					<OrderPageRow
						key={bid[OrderValue.Price]}
						bid={bid}
						highestTotal={highestTotal}
					/>
				))}
			</div>
		</div>
	);
}

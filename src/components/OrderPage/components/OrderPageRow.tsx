import React, { memo } from "react";

import { TOrderExtended } from "@base/types";
import { OrderValue } from "@base/consts";

import style from "../OrderPage.module.scss";

export const OrderPageRow = memo(
	({ bid, highestTotal }: { bid: TOrderExtended; highestTotal: number }) => (
		<div className={style.orderPageRow}>
			<div
				className={style.percentage}
				style={{
					width: `${(bid[OrderValue.Total] / highestTotal) * 100}%`,
				}}
			/>
			<div className={style.price}>
				{bid[OrderValue.Price].toLocaleString(undefined, {
					minimumFractionDigits: 2,
				})}
			</div>
			<div>{bid[OrderValue.Size].toLocaleString()}</div>
			<div>{bid[OrderValue.Total].toLocaleString()}</div>
		</div>
	),
);

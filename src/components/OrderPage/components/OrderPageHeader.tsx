import React, { memo } from "react";

import style from "../OrderPage.module.scss";

export const OrderPageHeader = memo(() => (
	<div className={style.orderPageHeader}>
		<div>PRICE</div>
		<div>SIZE</div>
		<div>TOTAL</div>
	</div>
));

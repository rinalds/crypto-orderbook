import { Button } from "@components/Button/Button";
import React from "react";

import style from "./Popup.module.scss";

interface PopupProps {
	text: string;
	buttonTitle: string;
	onClose: () => void;
}

export const Popup = ({ text, buttonTitle, onClose }: PopupProps) => (
	<div className={style.popupContainer}>
		<div className={style.popup}>
			<p>{text}</p>
			<Button title={buttonTitle} onClick={onClose} />
		</div>
	</div>
);

import React, { memo } from "react";

import style from "./Button.module.scss";

interface ButtonProps {
	title: string;
	onClick: () => void;
}

export const Button = memo(({ title, onClick }: ButtonProps) => (
	<button type="button" className={style.button} onClick={onClick}>
		{title}
	</button>
));

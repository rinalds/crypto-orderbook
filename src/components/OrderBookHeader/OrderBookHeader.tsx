import classNames from "classnames";
import React from "react";

import style from "./OrderBookHeader.module.scss";

interface HeaderProps {
	spread: string;
	onlySpread?: boolean;
	hideSpread?: boolean;
}

export const OrderBookHeader = ({
	spread,
	onlySpread = false,
	hideSpread = false,
}: HeaderProps) => (
	<div
		className={classNames(style.orderBookHeader, {
			[style.onlySpread]: onlySpread,
		})}
	>
		{!onlySpread && <h1>Order Book</h1>}
		{!hideSpread && <h2 className={style.spread}>{`Spread ${spread}`}</h2>}
	</div>
);

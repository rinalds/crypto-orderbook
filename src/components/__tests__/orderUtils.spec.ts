import {
	calculateHighestTotal,
	convertOrdersToOrdersMap,
	extendOrdersWithTotal,
	updateOrderMap,
} from "@base/orderUtils";
import { TOrders } from "@base/types";

const orders: TOrders = [
	[30000, 1100],
	[35000, 1200],
	[40000, 1300],
	[45000, 1400],
	[50000, 1500],
	[55000, 1600],
	[60000, 1700],
	[65000, 1800],
	[70000, 1900],
	[75000, 2000],
];

const newOrders: TOrders = [
	[30000, 0],
	[50000, 4000],
];

describe("convertOrdersToOrdersMap", () => {
	it("should convert orders sent from server to order map", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);
		expect(ordersMap.get(35000)).toEqual([35000, 1200]);
		expect(ordersMap.size).toEqual(orders.length);
	});
});

describe("extendOrdersWithTotal", () => {
	it("should convert orders sent from server to order map", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);

		const extendedOrders = extendOrdersWithTotal(ordersMap, true, 10);

		expect(extendedOrders[0]).toEqual([30000, 1100, 1100]);
		expect(extendedOrders[9]).toEqual([75000, 2000, 15500]);
	});

	it("should sort orders in descending order if ascending=false", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);

		const extendedOrders = extendOrdersWithTotal(ordersMap, false, 10);

		expect(extendedOrders[0]).toEqual([75000, 2000, 2000]);
		expect(extendedOrders[9]).toEqual([30000, 1100, 15500]);
	});

	it("should slice array based on numberOfRows and based on new order calculate totals", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);

		const extendedOrders = extendOrdersWithTotal(ordersMap, true, 5);

		expect(extendedOrders.length).toBe(5);
		expect(extendedOrders[0]).toEqual([30000, 1100, 1100]);
		expect(extendedOrders[4]).toEqual([50000, 1500, 6500]);
	});
});

describe("calculateHighestTotal", () => {
	it("should calculate highest total from orders map", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);
		const extendedOrders = extendOrdersWithTotal(ordersMap, true, 10);
		const highestTotal = calculateHighestTotal(extendedOrders);

		expect(highestTotal).toBe(15500);
	});
});

describe("updateOrderMap", () => {
	it("should update ordersMap with based on new orders", () => {
		const ordersMap = convertOrdersToOrdersMap(orders);

		expect(ordersMap.get(30000)).toEqual([30000, 1100]);
		expect(ordersMap.get(50000)).toEqual([50000, 1500]);

		updateOrderMap(newOrders, ordersMap);

		expect(ordersMap.get(30000)).toBeUndefined();
		expect(ordersMap.get(50000)).toEqual([50000, 4000]);
	});
});
